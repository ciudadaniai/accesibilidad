# Accesibilidad de la Documentación

En este documento vamos a tratar los distintos aspectos de la **documentación técnica**[^1].

* **Qué es**
* **Por qué es importante que** la documentación técnica **sea accesible**.
* **Aspectos** de una buena documentación técnica
* **Reglas** que queremos fijar para mejorar la accesibilidad de todos nuestros proyectos
* **Herramientas** que podemos usar para ayudarnos en este objetivo
* **Ideas** que podrían ser útiles para este objetivo.

## Qué es

La documentación técnica es un manual de instrucciones de nuestro proyecto.

  > Cambia la situación de "lee el código" a "así funciona el proyecto"

Ayuda a nuevos desarrolladores a entrar al proyecto, y nos permite retomar un proyecto luego de que los autores originales ya no están presentes. Le da una cierta permanencia que no podemos obtener de otro modo.

Dada su importancia, la documentación técnica debe ser accesible. A continuación describimos más razones que refuerzan esta idea.

## Razones

Motivos para hacer nuestra documentación más accesible son muchos:

* Cuando creamos herramientas, queremos que éstas lleguen a tantos desarrolladores como sea posible.
* No todos los programadores pueden elegir su sistema operativo:
    * Desarrollador Web? &rarr; Linux o Mac OS
    * Ciego? &rarr; Windows o Mac OS[^2]
    * Con bajo presupuesto? &rarr; PC &rarr; Windows o Linux
    * Entre muchos otros!
    Por lo tanto, es nuestro deber tratar de darle soporte en la documentación a **tantos sistemas como nuestro proyecto permita**.
* Escribimos la documentación los unos para los otros. La documentación no es un tema del "usuario", es un tema de **pares**.
* La documentación técnica es **crucial** para los desarrolladores que son nuevos en el proyecto. No sólo para poder trabajar en el proyecto, si no que para entender cómo fue construido y cómo es que funciona.
* La documentación técnica **ayuda especialmente a los desarrolladores remotos**. Todavía más si son nuevos al proyecto.

## Aspectos de una buena documentación técnica

Buena documentación técnica:

* Está **localizada** al lenguaje del lector objetivo.
* Es **clara** en su lenguaje.
* Permite **búsqueda** de texto.
* Es accesible para **usuarios con disabilidades**.
* **Deja claro** qué le puede entregar al lector y qué no.
* Está *al día*.
* Es **mantenible**.
* **Le da poder a su lector**.

## Reglas para los proyectos de FCI

Ahora que está claro qué es la documentación técnica, por qué queremos que sea accesible y qué características debiéramos poder observar en una buena DT, a continuación proponemos las siguientes reglas a cumplir en la documentación de nuestros proyectos:

* En el **lenguaje**, evitar los coloquialismos y lenguaje "interno". Del video de Mariko Kosaka:[^3]
  > Instead of "Wait for a LGTM! from a maintainer", say "Wait for a maintainer to look at it and approve it". *LGTM* is not something that everyone understands, specially users from different cultures.
* **Ceguera, daltonismo y condiciones vestibulares** deben ser consideradas a la hora de usar diagramas. Por ejemplo:
    * *Funciona este diagrama sin colores?*
    * Proveer descripciones para usuarios ciegos
    * Evitar animaciones fuertes que puedan generar episodios de epilepsia o de vértigo.
* Videos deben tener **subtítulos**
* Audio debe de tener **_transcript_**
* Todo lo que no está soportado por el proyecto (como un sistema operativo) **debe ser explicitado**.
* La documentación técnica debiera permitirle al lector:
    * **Instalar localmente** para el desarrollo.
    * **Configurar e instalar en** el servidor de **producción**.
    * Obtener una **vista general de la arquitectura** del proyecto.
* Debiera haber en el directorio principal del proyecto:
    * Un archivo `README.md`, que presenta brevemente el proyecto e incluye *links* a toda la documentación del proyecto.
    * Un archivo `LICENSE`, que explicita bajo qué licencia estamos desarrollando y publicando el proyecto.
* Si el proyecto fue diseñado para ser adoptado por otras organizaciones, una **guía clara de adopción** debiera estar presente:
    * "Cómo traducir la interfaz?"
    * "Cómo incluir los datos de tu país / espacio demográfico?"
    * "Cómo extender el proyecto con nuevas funcionalidades?"
* Almacenar la documentación en **el mismo repositorio** que el proyecto.
  > Así, la documentación es más fácil de mantener al día

## Herramientas

Para hacer nuestra documentación más accesible, a continuación describimos herramientas que nos podrían ser muy útiles:

* **Para escribir de forma clara** (en inglés)[^4]: [Hemingway App](http://www.hemingwayapp.com/)
* Para generar documentación con **estructura accesible**: [HTML Semántico](https://www.hongkiat.com/blog/html-5-semantics/)
* **Testeo automático** de todas las plataformas soportadas por el proyecto. Así, nos aseguramos de que las instrucciones de nuestra documentación siguen siendo válidas y que no hemos roto nada para alguna al hacer un cambio. **Integración Continua** es una gran herramienta para facilitar esto.
* Plataformas de documentación:
    * [MkDocs](https://www.mkdocs.org/) para generar sitios de documentación usando _Markdown_
    * [Sphinx](http://www.sphinx-doc.org/en/master/) para lo mismo usando _ReStructuredText_
    * [Read the Docs](https://readthedocs.org/), muy usado en proyectos Open Source, que puede recibir sitios de MkDocs y Sphinx como input.

## Ideas para hacer el proyecto en si mismo más accesible

* Archivos `.ignore` para **reducir el tamaño** del repositorio.
  > Permiten que usuarios con menos recursos / infraestructura que nosotros puedan descargar y usar nuestro proyecto con mayor facilidad. Útil sobre todo con los assets del entorno de producción.
* Escribir los *scripts* de instalación en el **mismo lenguaje** que el proyecto
  > Facilita la instalación en distintas plataformas.
* **Testear** periódicamente **en máquinas más lentas** / con menos recursos que las que usamos en desarrollo.
  > No todos los equipos de desarrollo tienen nuestro presupuesto.    
  > No todos los usuarios tienen la última generación de dispositivos.
* Incluir **muestras o generadores de datos** en el repositorio.
  > Facilita la entrada al proyecto por parte de nuevos desarrolladores y organizaciones.  
  > Facilita el testeo local de funcionalidades.    
  > Facilita el testeo automático del proyecto.

## Bibliografía / Notas al Pie

[^1]: A diferencia de la documentación de usuario

[^2]: Lamentablemente las herramientas de accesibilidad como *screen readers* no están tan avanzadas en Linux como en otros sistemas operativos

[^3]: [Re-inventing the Rosetta Stone Together - Mariko Kosaka](https://www.youtube.com/watch?v=OOzAly5Rs7g)

[^4]: Encontrar algo como esto para español y portugués nos sería increíblemente útil. A la fecha, no he encontrado nada como Hemingway, pero hay que mantener los ojos abiertos.