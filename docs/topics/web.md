# Accesibilidad Web

## Introducción

### Objetivos de este documento

La accesibilidad web es muy importante hoy en día. Existen personas con problemas motrices, personas que no ven, personas con problemas de visión a color. Existen personas que hablan distintas lenguas, existen personas que no pueden usar manos para interactuar con el computador. Es por esto, entre otras cosas, que queremos hacer nuestros proyectos más accesibles: no es posible que siendo FCI estemos ignorando las necesidades de estas personas, que suelen ser también las que más necesitan de ayuda.

Hay otras razones para mejorar la accesibilidad: hacer un sitio más amigable para lectores de pantalla nos permite interpretarlo mejor con herramientas automatizadas para, por ejemplo, exportarlo a un archivo o crear un mapa de contenidos. 

### Límites de este documento

Este documento es un trabajo iterativo, y por el momento tratar de abarcar la [guía completa](https://www.w3.org/TR/WCAG21/) del W3C[^who_is_w3c] es algo inalcanzable en el corto plazo. Por lo tanto, más que un trabajo “exhaustivo”, es mejor considerarlo como un best effort: el documento va a mostrar lo que hemos priorizado en aprender e implementar más que mostrar una guía completa de cómo cumplir con el estándar de la W3. Llegar a cubrir el estándar completo es un objetivo a largo plazo, por supuesto, pero antes de llegar a ello preferimos cubrir y dominar las partes que mejor podamos lograr e ir construyendo sobre ese progreso.

## Vista Superior del documento

En este documento vamos a abarcar la implementación de accesibilidad desde 2 frentes principales: 

* **Accesibilidad desde día 0**: o “accesible por construcción”. Hay algunos puntos de accesibilidad que son mucho más fáciles de abarcar al comienzo de un proyecto. La idea de “accesible por construcción” es la idea de que desde el comienzo de su desarrollo, el proyecto cumpla con algún requisito de accesibilidad, y que esto se mantenga hasta el término del proyecto.
* **Accesibilidad a posteriori**: o “testear y mejorar la accesibilidad”. Cuando un proyecto ya ha empezado, o cuando las circunstancias no nos permiten abarcar un requisito de accesibilidad desde el comienzo, necesitaremos revisar (testear) esos requisitos durante el desarrollo del proyecto, para intentar mejorarlos antes de que se termine el proceso.

Ambas son importantes, porque:

* Hay cosas que la **accesibilidad a posteriori** no puede realmente mejorar sin incurrir en un gasto tremendo de recursos (como por ejemplo, cambiar una paleta de colores después de que el sitio entero esté diseñado y que la contraparte lo haya aprobado).
* Hay situaciones en las que la **accesibilidad desde día 0** no es realmente posible: cuando la contraparte cambia un requisito que nos hace agregar localización a un sitio que no la necesitaba anteriormente, o cuando heredamos un proyecto que ya está construido o en proceso de construcción.
 
También vamos a abarcar la **mantención** de la accesibilidad a través del tiempo: 

* ¿Cómo nos aseguramos de que las mejoras que hemos agregado no se pierdan o degraden a través del tiempo? 
* ¿Qué partes debemos chequear manualmente y qué partes podemos automatizar?

## Accesibilidad desde Día 0

Como fue descrito en el punto anterior, la idea de **Accesibilidad desde Día 0** es que desde el comienzo del proyecto se cumpla con los requisitos de accesibilidad. Esto agrega más carga en el día a día al equipo, por lo que debemos tenerlo en cuenta a la hora de estimar los tiempos y costos. Sin embargo, el cumplir con accesibilidad desde el primer día suele ser **mucho más eficiente** que agregar el cumplimiento a posteriori, por lo que recomendamos tomar esta opción siempre que sea posible.

Dado esto, ¿cómo cumplimos con accesibilidad desde el día 0? A continuación voy a dar dos ejemplos notables, pero el mismo concepto se puede extender a otros requisitos de accesibilidad que se vayan haciendo evidentes al adquirir más experiencia trabajando en esta área.

### Paleta de Colores

La diferencia entre cambiar la paleta de colores al principio y cambiarla al final es gigantesca. Tomemos como caso de estudio en accesibilidad al proyecto [Kosovo Trust Building](https://kosovotrustbuilding.com/) de la Fundación. Si bien desafíos como la localización, etiquetas _ARIA_[^aria] a enlaces e imágenes y la estructura de los _headings_ del DOM[^dom] se pudieron mejorar a posteriori, mejorar los problemas de contraste de la paleta resultó infeasible: el equipo de diseño estaba muy ocupado y la contraparte ya aprobó el sitio entregado. Un cambio tan fundamental al mismo como la paleta de colores requeriría 1 mes extra de trabajo para arreglar algo que idealmente habría sido resuelto en las primeras semanas de trabajo.

El criterio de accesibilidad del W3C establece límites inferiores de contraste entre el color de fondo y el color de letra[^lower_bound_of_contrast_spec]. Dado un tamaño de fuente y un estándar a seguir (AA o AAA), es posible revisar si una combinación de colores cumple con los requisitos del estándar. Herramientas de [WebAIM](https://webaim.org/) como el [Color Contrast Checker](https://webaim.org/resources/contrastchecker/) son muy útiles para este propósito, y podemos usarlas para modificar una paleta de colores deseada hasta que ésta cumpla con los requisitos de accesibilidad.

Sin embargo, más fácil que revisar si una paleta de colores es accesible es generar una que lo sea desde el comienzo. Para ello una buena opción es usar la herramienta [ColorSafe](http://colorsafe.co/). Sea cual sea la herramienta, recomendamos encarecidamente que desde el principio se utilice una paleta accesible. Eso nos permitirá trabajar con tranquilidad, sabiendo que la base del diseño no va a cambiar y dándonos un empujón tremendo en accesibilidad desde el principio.

### Localizar Todo al Escribirlo

En una página web, el texto se dice que está _localizado_ si es que la app lo ofrece en más de 1 idioma o "locale". En muchos proyectos de la fundación, especialmente aquellos que atienden a comunidades multiculturales, la localización de la app a todos los idiomas de la comunidad objetivo es un requisito de accesibilidad.

Sin embargo, es muy distinto establecer la localización a priori que agregarla una vez ya el desarrollo está avanzado. En particular, el trabajo de encontrar _strings_ (texto) no localizado en el código fuente de una app con el fin de localizarlo, suele ser un trabajo lento y muy difícil de terminar. 

Por otro lado, existen lenguas para las que localizar el texto **no es suficiente**! En ellas, el _layout_ y la _UI_ de la app deben adaptarse al idioma en que se está usando. Para el idioma **japonés** por ejemplo, la dirección del texto puede ser vertical, y el diseño de los documentos sigue líneas de estética distintas a las que el público occidental espera[^japanese_web_aesthetics]. El W3C tiene de hecho un [documento](https://www.w3.org/TR/jlreq/) donde se detalla cada aspecto del diseño web para este idioma.

Otro idioma que sirve como ejemplo es el **árabe**. No sólo el texto debe ser escrito de derecha a izquierda: **toda la página debe ser reflejada**[^basic_arab_ux]. También es importante considerar que la sociedad árabe-parlante es más conservadora y tiene en general, distintos valores que los de la sociedad occidental. Es por esto que el contenido de la página también necesita un cierto nivel de **localización cultural**[^designing_an_arabic_ux] que normalmente no es necesario al localizar por ejemplo, del inglés al español.

Para casos como éstos, mientras más tarde se agregue la localización, más trabajo de diseño habrá que hacer de nuevo simplemente porque estos escenarios requieren de una interfaz más flexible de lo que estamos acostumbrados a entregar.

Por lo tanto, si empezamos localizando la app desde el día 0, nos podremos ahorrar mucho trabajo posterior pues el requisito ya está siendo cumplido. Las contrapartes también estarán más satisfechas, pues podrán revisar cómo se ve la página en los distintos _locales_ desde mucho antes que si los fuésemos agregando al final.

## Accesibilidad a Posteriori

Hay veces en las que queremos mejorar la accesibilidad de un proyecto que ya empezamos. Para ello, podemos revisar la app para saber qué mejorar. Al proceso de revisar un proyecto le llamamos __Audit__. Normalmente, un Audit revisa el proyecto de forma general y nos dice qué es lo que podemos mejorar.

Cuando ya sabemos qué es lo que necesitamos mejorar, podemos buscar una herramienta específica para buscar problemas en esa área.

### Herramientas de Revisión Automatizada

Para hacer Audits al proyecto de forma general, existen 2 herramientas automatizadas muy buenas que se complementan entre sí: _Lighthouse_ y _WAVE_. 

Antes de considerar un proceso de "Revisión de Accesibilidad" completado, es una buena idea revisar el veredicto de ambas herramientas. El uso de cada una no toma más de 1 minuto, por lo que capturar errores con ellas es muy eficiente.

Es importante señalar que las herramientas no revisan el sitio completo, si no que sólo la página entregada. Por ejemplo, si se quiere revisar la página de Kosovo Trust Building desarrollada por FCI, es necesario no sólo ejecutar las herramientas sobre `kosovotrustbuilding.com`, si no que también es importante hacerlo sobre `kosovotrustbuilding.com/en/about`, `kosovotrustbuilding.com/en/start`, etc.

#### Lighthouse

La herramienta Lighthouse[^lighthouse_wikipedia][^lighthouse_chrome] es una herramienta de Google que es capaz de revisar nuestra página en diversos aspectos: _Performance, Accessibility, Best Practices, SEO_. En nuestro caso, nos interesa más que nada Accessibility. La sección de SEO también muestra algunos aspectos de accesibilidad, así que es mejor usar ambas.

Para usar Lighthouse, basta con entrar al sitio `https://web.dev/measure`. Pero para usarla localmente, podemos utilizar también el complemento para Firefox[^lighthouse_report_generator].

La herramienta genera _Reports_ de la página entregada. Cuando encuentra un problema, muestra **tips** sobre cómo solucionarlo y un link a una **explicación** del problema en la guía de accesibilidad de Google.

#### WAVE _(Web Accessibility Evaluation Tool)_

La herramienta WAVE[^wave_main_page] es una herramienta de WebAIM[^webaim_main_page] que modifica visualmente la página que revisa, para **mostrarnos** exactamente **dónde** es que encuentra problemas. Es mucho más exhaustiva que Lighthouse, pero de todos modos ambas a veces se complementan.

Es posible usarla desde la página de ellos, pero también es posible usarla desde un complemento para Firefox[^wave_ff_extension].

### Revisión Manual

Hay veces en las que no podemos revisar nuestros requisitos de accesibilidad automáticamente. Quizás hay algo que las herramientas no son capaces de ver, y por lo tanto no lo evalúan, o quizás hay contexto en la página que tiene sentido para un humano pero la herramienta no lo puede ver. Sea cual sea la situación, es bueno tener en cuenta que algunas cosas de accesibilidad **tendrán que ser revisadas a mano**.

A continuación describimos un ejemplo que vimos al trabajar en el proyecto de Kosovo.

#### Caso: buscando texto no-localizado en Rails

> Esta sección requiere conocimiento técnico

##### Proceso

Durante el desarrollo del proyecto KTB, hubo mucho texto que no fue localizado al agregarlo a la app. Para localizar la app, fue necesario buscar a través de todos los archivos del proyecto por la presencia de un `string` que estuviese escrito en inglés. El proceso a seguir para localizar la app fue el siguiente:

* Instalar la gema `i18n-tasks`[^i18n_tasks_gem] para revisar que todos los idiomas tuviesen localizaciones para cada llave de traducción.
    * Buscar strings escritos en inglés en la app. Para cada uno que fue encontrado:
        * Expresarlo como una llamada a `i18n` (es decir, algo como `t(‘.button.yes’)`)
        * Agregar la llave y el string original al archivo del _Locale_ del inglés.
        * Usar `i18n-tasks` para **propagar la nueva llave** a los otros archivos de Locale del proyecto.
    * Repetir el paso anterior hasta que no se logren encontrar más strings en inglés.

##### Lecciones aprendidas

De este esfuerzo aprendimos lo siguiente:

1. Es posible que existan herramientas que ayuden sustancialmente a trabajar en los problemas de accesibilidad (como la gema `i18n-tasks`). 
     * Antes de intentar resolver el problema manualmente, es una buena idea revisar si existe una herramienta para automatizarlo.
2. Buscar strings repartidos por el proyecto es más difícil de lo que parece.
    * Herramientas de Unix como `grep` ayudan bastante
    * Al escribir un string, dejarlo expresado como una llamada al Locale ahorra mucho tiempo posterior de buscarlo

### Revisión Profesional, o Directa

Una alternativa a considerar para los proyectos de la Fundación es encontrar y contratar como _freelancers_ o con el modelo que mejor funcione, a usuarios de tecnologías de accesibilidad (como _screen readers, voice controls_ o _high contrast palettes)_. 

La idea sería pedirles que revisen la usabilidad de nuestros proyectos desde su propia experiencia. Dado que **ninguno de nosotros tiene _expertise_** en uso de tecnologías de accesibilidad, la ayuda de alguien que **las use en el día a día** podría ser invaluable dependiendo de la situación.

## Mantención de la Accesibilidad

Una cosa es implementar accesibilidad en un proyecto desde el comienzo, o quizás agregarla posteriormente. Pero cuánto nos toma mantener estos requisitos activos en uno de nuestros proyectos? En un mundo ideal, con infinitos recursos, le haríamos un _audit_ manual a la aplicación antes de subir cualquier cambio. Esto obviamente no es posible, así que es necesario llegar a un término medio que funcione para la Fundación.

Afortunadamente, es posible utilizar Integración Continua[^wikipedia_ci] o CI (de _Continuous Integration)_ en nuestros proyectos. Usar CI nos permite ejecutar _tests_ automáticamente antes de incluir nuevos cambios en nuestros proyectos. Si los tests no pasan, los cambios son devueltos al usuario junto con retroalimentación que indica qué es lo que faltó.

El testeo automático es ideal para **mantener el nivel** de accesibilidad previamente **logrado**. A continuación incluyo un ejemplo de un test que pudimos automatizar para el proyecto de Kosovo, relacionado con la localización de la app.

### Ejemplo: *i18n-tasks health*

> Para mantener un nivel logrado de localización en Ruby on Rails

Este comando, de la gema `i18n-tasks`, evalúa 4 cosas:

* Si en el proyecto hay llamadas a `i18n.t` con llaves que no existen en los archivos de localización (es decir, **si faltan traducciones**)
* Si en el proyecto todas las llaves de los archivos de localización están siendo usadas (es decir, **que no haya traducciones que no se usen**)
* Que no hayan llaves mal escritas en el proyecto (es decir, **que** todos **los archivos de localización sean válidos**)
* Que los archivos de localización estén normalizados (básicamente, que las llaves estén ordenadas alfabéticamente)

Este comando nos permite entonces evaluar que nuestros archivos de localización no estén incompletos. Al instalar la gema en el proyecto, se agrega esta evaluación al _script_ ya existente de testeo del proyecto, con lo que automáticamente aparece como un test en CI. 

**En repetidas ocasiones** sucedió que estábamos subiendo cambios donde faltaban traducciones, o los archivos estaban desordenados. En lugar de permitirnos subir estos cambios incorrectos a la página, el test rechazó los cambios y nos señaló dónde estaba el problema automáticamente.

### Otros Ejemplos

Otras herramientas que es posible ejecutar en CI son Lighthouse[^lighthouse_bot] y WAVE[^wave_standalone]. En las notas al pie aparecen detalles sobre cómo llevar esto a cabo.


## Trabajo a Futuro

Este documento está incompleto. Cosas a agregar en el futuro podrían ser:

* Más ejemplos de _Accesibilidad desde Día 0_
* Más herramientas automatizadas de revisión de accesibilidad a posteriori
* Ejemplos de _Revisión Manual_ que no se pueden revisar con herramientas automáticas
* Explicaciones de cómo algunas prácticas de accesibilidad ayudan a los usuarios para que el lector entienda algunas partes del estándar en su contexto. Ejemplo: en WAVE, por qué existe la regla de "Never Skip Heading Levels" sobre los _headings_ del HTML.
* Revisar la posibilidad de usar _Tenon[^tenon_homepage]_ en CI. Mientras más herramientas tengamos disponibles, mejor. 


## Bibliografía / Notas al Pie

[^who_is_w3c]: [About W3C - World Wide Web Consortium](https://www.w3.org/about)

[^aria]:  [ARIA - Accessibility | MDN - Mozilla](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA)

[^dom]: [Domain Object Model](https://es.wikipedia.org/wiki/Document_Object_Model)

[^lower_bound_of_contrast_spec]: [Understanding Success Criterion 1.4.3 | Understanding WCAG 2.0](http://www.w3.org/TR/UNDERSTANDING-WCAG20/visual-audio-contrast-contrast.html)

[^japanese_web_aesthetics]: [Designing Japanese documents - tcworld.info - content strategies](http://www.tcworld.info/e-magazine/content-strategies/article/designing-japanese-documents/)

[^basic_arab_ux]: [Designing for the Arab User — Basic Arabic UX for Business](https://www.freecodecamp.org/news/designing-for-the-arab-user-basic-arabic-ux-for-business-6ff29d4c7c60/)

[^designing_an_arabic_ux]: [Designing an Arabic UX: Usability & Arabic User Interfaces - Uxbert](http://uxbert.com/designing-an-arabic-user-experience-usability-arabic-user-interfaces/)

[^lighthouse_wikipedia]: [Google Lighthouse - Wikipedia](https://en.wikipedia.org/wiki/Google_Lighthouse)

[^lighthouse_chrome]: [lighthouse - GitHub](https://github.com/GoogleChrome/lighthouse)

[^lighthouse_report_generator]: [Lighthouse Report Generator – Extension for Firefox](https://addons.mozilla.org/en-US/firefox/addon/lighthouse-report-generator/)

[^wave_main_page]: [WAVE Web Accessibility Tool - WebAIM](https://wave.webaim.org/)

[^webaim_main_page]: [WebAIM: Web Accessibility In Mind](https://webaim.org/)

[^wave_ff_extension]: [WAVE Accessibility Extension – Extension for Firefox](https://addons.mozilla.org/en-US/firefox/addon/wave-accessibility-tool/)

[^i18n_tasks_gem]: [i18n-tasks - Github](https://github.com/glebm/i18n-tasks)

[^wikipedia_ci]: [Continuous integration - Wikipedia](https://en.wikipedia.org/wiki/Continuous_integration)

[^lighthouse_bot]: [lighthousebot: Run Lighthouse in CI](https://github.com/GoogleChromeLabs/lighthousebot)

[^wave_standalone]: [WAVE Stand-alone API](https://wave.webaim.org/standalone)

[^tenon_homepage]: [Tenon: Accesibility as a Service](https://tenon.io/)