# Accesibilidad FCI

Esta página contiene una serie de documentos relacionados con la accesibilidad, para uso dentro de la Fundación Ciudadanía Inteligente.

## [Desarrollo](topics/desarrollo.md)

El desarrollo de nuestros proyectos debe ser accesible a todos los miembros del equipo y, ya que son proyectos abiertos, también deben serlo para todos los que lo vayan a modificar o mantener en el futuro.

## [Web](topics/web.md)

Nuestros proyectos Web son usados por cientos, si no miles, de personas. Personas de todas las edades, culturas y necesidades. Existen buenas prácticas y tecnologías modernas con las que podemos hacer que nuestros proyectos puedan ser accesibles a todas ellas.

## [Diseño](topics/diseño.md)

TODO: fill