### Acerca de

> This part is a Work in Progress

En la Fundación Ciudadanía Inteligente (FCI) desarrollamos tecnología para fortalecer las democracias en América Latina. Hemos desarrollado plataformas que fiscalizan autoridades, acercan comunidades a sus gobiernos, luchan contra la corrupción, fortalecen el trabajo de activistas, y permiten la incidencia en políticas públicas.

Buscamos ser una organización inclusiva que lucha por la justicia social, y es por ello que la Accesibilidad Web se vuelve relevante, ya que permite que todas y todos puedan acceder a las herramientas que construimos. 

Entendemos dicha Accesibilidad Web de una forma amplia, como la capacidad de acceso a la Web y a sus contenidos por todas las personas, independientemente de la discapacidad (física, intelectual o técnica) que presenten o de las que se deriven del contexto de uso (tecnológicas o ambientales)[^def_acc_web]. 

Por otro lado, al desarrollar código abierto también entendemos que nuestro código debe ser accesible para desarrolladoras y desarrolladores[^acc_dev_tools_guide].

[^def_acc_web]: [Definición de accesibilidad web](http://accesibilidadweb.dlsi.ua.es/?menu=definicion)

[^acc_dev_tools_guide]: [A guide to coding accessible developer tools – Increment: Development.](https://increment.com/development/a-guide-to-coding-accessible-developer-tools/)