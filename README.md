# Docs de Accesibilidad de FCI

Estamos escribiendo estos documentos usando Markdown, y rendereándolos como página web usando [MkDocs](https://www.mkdocs.org).

## Setup

### Edición básica

Los documentos están escritos en [Markdown](https://markdown.es/sintaxis-markdown/). 

Estamos usando una extensión de la sintaxis original que nos permite agregar notas al pie de página: [MD footnote extension](https://www.markdownguide.org/extended-syntax/#footnotes).

Para ajustar el estilo de las páginas, modificar el archivo `docs/css/extra.css`.

### Previsualizar

Para poder renderear la página localmente, necesitamos [instalar MkDocs](https://www.mkdocs.org/#installation).

Las dependencias son las siguientes:

1. Tener `python` instalado
2. Tener `pip` instalado

Una vez se tiene `mkdocs` instalado, podemos previsualizar la página usando el comando `mkdocs serve` en este directorio.

### Edición avanzada

Para cambiar el tema, agregar páginas o cambiar la navegación de la página, seguir las instrucciones que hay desde [aquí](https://www.mkdocs.org/#getting-started) en adelante.

## Deploy

La página se _deployea_ automáticamente a https://accesibilidad.readthedocs.io/es/latest/ apenas se actualiza la rama `master` del repo.
